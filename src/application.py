import getopt
import json
import sys
from json import JSONDecodeError

from slackclient import SlackClient


def main(argv):
    usage_str = 'python src/slack-notifier.py -f config.json -m "Hello from Server :tada:" -v true'
    try:
        opts, args = getopt.getopt(argv, "hm:v:f:", ["message=", "verbose=", "file="])
    except getopt.GetoptError:
        print(usage_str)
        sys.exit(2)

    message = None
    config_file_path = None
    verbose = False

    for opt, arg in opts:
        if opt == '-h':
            print(usage_str)
            sys.exit()

        elif opt in ('-m', '--message'):
            message = arg

        elif opt in ('-f', '--file'):
            config_file_path = arg

        elif opt in ('-v', '--verbose'):
            verbose = arg.lower() in (1, 'true', 'yes')

    if not config_file_path and not config_file_path:
        print('Err! Please provide config file path')
        sys.exit()

    if not message and not message:
        print('Err! Please provide message to send')
        sys.exit()

    try:
        with open(config_file_path, 'r') as f:
            config = json.load(f)

        if not config['API_TOKEN']:
            print('Err! Please provide API Token in config file')
            sys.exit()

        if not config['CHANNEL']:
            print('Err! Please provide Channel ID in config file')
            sys.exit()

        slack_client = SlackClient(config['API_TOKEN'])
        kwargs = {
            'channel': config['CHANNEL'],
            'text': message,
        }

        if config['USERNAME']:
            kwargs['username'] = config['USERNAME']

        if config['ICON_URL']:
            kwargs['icon_url'] = config['ICON_URL']

        if config['AS_USER']:
            kwargs['as_user'] = True

        response = slack_client.api_call("chat.postMessage", **kwargs)

        if verbose:
            print(json.dumps(response, indent=2))

        if not response['ok']:
            print('Err! Message sending failed')
            sys.exit()

    except IOError as ioe:
        print('Error while parsing the file. Err: {}'.format(ioe))

    except JSONDecodeError as jde:
        print('Error while parsing the file. Err: {}'.format(jde))


if __name__ == '__main__':
    main(sys.argv[1:])
