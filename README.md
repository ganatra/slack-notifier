# Slack Notifier

Small app hacked to notify to slack channel!

usage: `python src/application.py ... [-f file= | -m message= | -v verbose=]`

Options and arguments:

*-v or verbose=*
verbosely execute program values= 1, true, yes

*-f or file=:*
json config file path; check sample config to create one

- API_TOKEN: you need to generate API token for your slack-team
- CHANNEL: procure channel ID of your slack channel where you want to publish the message to
- USERNAME: name of the bot while posting the message
- ICON_URL: url of the icon when sending as a bot
- AS_USER: true/false will be published as a user when sending the message, if true USERNAME and ICON_URL will be ignored

*-m or message=*
message you want to send to the channel


---
usage examples:

```
python src/application.py -f config.json -m "Hello from Server :tada:" -v true
```


##### Install:
- clone repository
- `cd slack-notifier`
- `pip install -r requirements.txt`